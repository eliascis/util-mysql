-- Obtener una cadena corta
select UUID_SHORT();

-- Obtener una fecha formateada por el cliente
SELECT DATE_FORMAT(?,'%d/%m/%Y %r') AS fecha_formateada;

-- Obtener una cadena aleatoria 
select SHA2(CONCAT(now(),RAND()),256);

-- Buscar sobre un campo Varbinary
select * from t057_canales 
		 where id_hash= unhex('db55bd4a5e6a1e60353cc6f927fb0bbe4')

-- Registros afectados luego de un UPDATE o DELETE
SELECT ROW_COUNT() into fue_anulado;	

-- Concatenar
set doc_propina = CONCAT(ptipo_doc_dest_propina,'-',pnum_doc_dest_propina);

-- Hora actual
 SELECT now() INTO hora_actual;

 -- Id Autoincremental generado luego de un INSERT
 SET id_cliente = LAST_INSERT_ID();

 -- Hacer un split de una cadena
 SELECT
			SUBSTRING_INDEX(rif,'-',1) ,
			SUBSTRING_INDEX(rif,'-',-1) into tipo_doc_rif, numero_doc_rif;

-- Hacer un Cast
CAST(valor_convertido AS CHAR);

-- Encriptar
INSERT INTO t023_usuario (id_hash,  clave)
  VALUES (UNHEX(SHA2(CONCAT(now(),RAND()),512)),  AES_ENCRYPT('pass123', UNHEX(SHA2(llave,512))));

-- Desencriptar
SELECT id	INTO u_id                
			FROM t023_usuario           
			WHERE indicador = pindicador
				AND AES_DECRYPT(clave,UNHEX(SHA2(llave,512))) = pclave;

-- Declarar un cursos
DECLARE v_cierres CURSOR FOR 
		SELECT * FROM JSON_TABLE(lista_cierres, '$[*]'
				COLUMNS(hash_cierre varchar(256) PATH "$")) AS cierres;

-- Generar un elemento JSON
SET salida = JSON_OBJECT('cod_respuesta',cod_respuesta,'nb_respuesta',nb_respuesta);        


